package ru.terrakok.gitlabclient.ui.my.mergerequests

import ru.terrakok.gitlabclient.R
import ru.terrakok.gitlabclient.ui.global.BaseFragment

/**
 * @author Konstantin Tskhovrebov (aka terrakok). Date: 13.06.17
 */
class MyMergeRequestsFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_my_merge_requests
}