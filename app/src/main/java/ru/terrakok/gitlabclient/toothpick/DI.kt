package ru.terrakok.gitlabclient.toothpick

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 09.07.17.
 */
object DI {
    const val APP_SCOPE = "app scope"
    const val MAIN_ACTIVITY_SCOPE = "main activity scope"
}